# M3_UF3_P1 Gestió d'usuaris

## Contingut

- [Funcionament](#funcionament)
- [Estructura](#estructura)

## Funcionament

El projecte simula una aplicació d’un gimnàs que guarda les dades dels usuaris, en crea de nous, modifica i bloqueja aquests usuaris. Al iniciar el programa, es mostra un menú amb les diferents opcions. Es pot seleccionar qualsevol opció o triar 0 per finalitzar el programa. Després de finalitzar l'opció, el programa torna al menú principal. Si s'escull una opció no vàlida, el programa mostra un missatge d'error i torna a mostrar el menú.

## Estructura

El projecte conté una carpeta amb tres directoris on modifiquem, afegim i guardem les dades; i una altra on tenim el codi dividit en diferents fitxers. 

Per un costat a la carpeta del codi trobem quatre fitxers: el main, el “menu”, el d’“importació” i el “backups”. En el main es on cridem totes les funcions que fan diferents tasques, es el cor de l’aplicació. Seguidament, el “menu” conté quatre funcions; la primera només mostra les opcions de menú al usuari; la segona crea un usuari nou i el guarda en un fitxer i genera un id epl usuari; la tercera modifica les dades d’un usuari i les substitueix i finalment la última bloqueja o desbloqueja un usuari amb un boleà. El d’”importacio” afegeix i genera un id per a cada nou usuari, segons la posició. Finalment el de “backups” guarda la data i tots els canvis realitzats. En aquest últim cas, si no s’executa aquesta opció, al tancar l’aplicació es fa un backup automàtic en el main. 

D’altra banda en una altra carpeta anomenada “dataFiles” guardem totes les dades dels usuaris, els backups realitzats, i una altra on es guarden els usuaris existents, els nous i després es junten en el fitxer anterior. En el directori backups es guarden aquests, en el de data tots els usuaris amb les seves respectives dades i id’s, i en el import es guarden els nous usuaris i després els guarda en el data.
