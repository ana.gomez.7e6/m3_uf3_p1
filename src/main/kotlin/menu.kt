import kotlin.io.path.Path
import kotlin.io.path.appendText
import kotlin.io.path.readLines
import kotlin.io.path.writeText

/**
 * mostrarMenu -> Funció que només mostra el menú d'inici i les seves opcions
 **/
fun mostrarMenu(){
    println("\n")
    println("""
        ____________________________________
        |MENÚ PRINCIPAL
        |1. CREA UN NOU USUARI
        |2. MODIFICA L'USUARI
        |3. BLOQUEJAR O DESBLOQUEJAR USUARI
        |4. IMPORTAR DADES
        |5. GUARDAR TOTES LES DADES
        |0. SORTIR
        |___________________________________
        Selecciona una opció:
    """.trimIndent())
}

/**
 * crearUsuari -> Funció que demana les dades de l'usuari necessaries, les guarda i genera un id per l'usuari nou a partir de l'últim número
 */
fun crearUsuari(){
    var id:Int
    println("BENVINGUT, SI US PLAU INTRODUEIX LES TEVES DADES:")
    println("Introdueix el teu nom complet:")
    val nomUsuari= readln().toString()
    println("Introdueix el teu número de móvil:")
    val movilUsuari = readln().toString()
    println("Introdueix el teu Email:")
    val mailUsuari = readln().toString()
    //Aquí comprobem quin és l'últim id que hi ha enregistrat i esta asignat a aquest usuari
    val userData = Path("dataFiles/data/userData.txt")
    val linies = userData.readLines()
    if (linies.size<1) id=1
    else{
        id = linies[linies.size-1].split(";")[0].toInt()+1
    }
    var nouUsuari="\n".plus(id.toString()).plus(";$nomUsuari;$movilUsuari;$mailUsuari;true;false")
    //Afegim el nou usuari
    userData.appendText(nouUsuari)
    println("\nBenvingut $nomUsuari, \nEl teu identificador del gimnàs és: $id")
}

/**
 * modificarUsuari -> Funció que modifica les dades d'un usuari en concret. Primer demana l'id, i a partir d'aquest, busca l'usuari corresponent i es modifiquen els camps necessaris
 */
fun modificarUsuari(){
    var noTrobat=true
    var nomUsuari:String=""
    var movilUsuari:String=""
    var mailUsuari: String=""
    val userData = Path("dataFiles/data/userData.txt")
    var linies = userData.readLines().toMutableList()
    println("MODIFICA EL TEU USUARI")
    println("Introdueix el teu identificador de gimnàs:")
    var id= readln().toInt()
    //buscar si l'usuari existeix i la posició del id, mostrant tots els index de "linies"
    for (i in linies.indices){
        if (linies[i].split(";")[0].toInt()==id){
            println("Introdueix el teu nom complet:")
            nomUsuari = readln().toString()
            println("Introdueix el teu número de telèfon:")
            movilUsuari = readln().toString()
            println("Introdueix el teu Email:")
            mailUsuari = readln().toString()
            var nouUsuari=id.toString().plus(";$nomUsuari;$movilUsuari;$mailUsuari;true;false")
            //busca la posició del id i li adjudica les noves dades
            linies[i]=nouUsuari
            //pasa una llista a un string
            var nouData=linies.joinToString(separator = "\n")
            //Elimina el que hi havia i escriu el que hem demanat
            userData.writeText(nouData)
            noTrobat=false
            break
        }
    }
    if (noTrobat) println("Aquest usuari no existeix")
    else println("\nDades modificades.\nNOM: $nomUsuari\nNÚMERO MOV.: $movilUsuari\nEMAIL: $mailUsuari")
}

/**
 * bloquejarUsuari -> Funció que bloqueja o desbloqueja un usuari. Demana l'id i bloquejarà o desbloquejarà l'usuari correspnent a aquest id
 */
fun bloquejarUsuari(){
    var noTrobat=true
    val userData = Path("dataFiles/data/userData.txt")
    var linies = userData.readLines().toMutableList()
    println("BLOQUEJA O DESBLOQUEJA EL TEU USUARI")
    println("Introdueix el teu identificador del gimnàs:")
    var id = readln().toInt()
    for (i in linies.indices){
        if (linies[i].split(";")[0].toInt()==id){
            var usuariBloq= linies[i].split(";")[5].toBoolean()
            usuariBloq=!usuariBloq
            //dona l'index de última coincidencia d'un carácter dins d'un string
            var index = linies[i].lastIndexOf(";")
            var nouUsuari=linies[i].substring(0,index).plus(";$usuariBloq")
            //busca la posició de l'id i li adjudica les noves dades
            linies[i]=nouUsuari
            //pasa una llista a un string
            var nouData=linies.joinToString(separator = "\n")
            //Elimina el que hi havia i escriu el que hem demanat
            userData.writeText(nouData)
            noTrobat=false
            if (usuariBloq==true) println("\nUsuari bloquejat")
            if (usuariBloq==false) println("\nUsuari desbloquejat")
            break
        }
    }
    if (noTrobat) println("Aquest usuari no existeix")
}