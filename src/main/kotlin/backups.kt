import java.text.SimpleDateFormat
import java.util.*
import kotlin.io.path.*

/**
 * ferBackup -> Funció que fa una còpia de seguretat del fitxer userData
 * @param ferCopia -> Per defecte és true, i serveix per saber si vols fer una copia en cas de que ja existeixi
 */
fun ferBackup(ferCopia:Boolean = true) {
    val dia = SimpleDateFormat("yyyy-MM-dd").format(Date())         //Aconseguim la data del dia actual en el format
    val dataCopy = Path("dataFiles/backups/".plus(dia).plus("userData.txt"))      //Obrim els fitxers de userData i del backup
    val userData = Path("dataFiles/data/userData.txt")
    if (ferCopia) dataCopy.deleteIfExists()                         //Si s'ha de fer la copia, esborrem el fitxer del backup
    if (!dataCopy.exists()) userData.copyTo(dataCopy)               //Si el fitxer no existeix, el creem a partir de userData
}