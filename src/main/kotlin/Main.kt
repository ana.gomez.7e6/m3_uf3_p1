/**
 * Aplicació d'un gimnás que gestiona els usuaris. Mostra un menú i cada opció farà:
 * Creació d'usuari -> demana dades al usuari (nom complet, telefon i mail), les guarda i genera un id per l'usuari.
 * Modificar usuari -> demana l'id del usuari i torna a demanar les dades anteriors, substituint-les per les que hi havia.
 * Des/Bloquejar usuari -> bloqueja o desbloqueja l'usuari
 * Importació de dades -> importa les dades afegides
 * Guardar dades -> fa un backup i es guarden totes les dades noves
 *
 * @author Elna Ballesta i Ana Gómez
 * @version 1.0
 */
fun main(args: Array<String>) {
    var opcio:Int
    importacioDades()
    do {
        mostrarMenu()
        try {
            opcio = readLine()!!.toInt()
        } catch (e: NumberFormatException) {
            opcio = -1
        }
        when (opcio) {
            1 -> crearUsuari()
            2 -> modificarUsuari()
            3 -> bloquejarUsuari()
            4 -> {
                importacioDades()
                println("S'han importat noves dades.")
            }
            5 -> {
                ferBackup()
                println("S'ha creat una còpia de seguretat.")
            }
            0 -> println("Sortint del programa")
            else -> println("Opció no vàlida")
        }
    } while (opcio != 0)
    ferBackup(false)
}