import java.nio.file.Path
import kotlin.io.path.*

/**
 * importacioDades -> funció que importa, si n'hi ha, els fitxers de la carpeta import al fitxer userData
 */
fun importacioDades(){
    val path = Path("dataFiles/import/")                  //Obrim el directori import i creem una llista amb els fitxers
    val files : List<Path> = path.listDirectoryEntries()
    for(item in files) {                //Per cada fitxer del directori, cridem la funció importarFitxer, i després s'elimina el fitxer
        importarFitxer(item)
        item.deleteIfExists()
    }
    val dataFile = Path("dataFiles/data/userData.txt")        //Finalment, si el fitxer userData, no existeix, el crea
    if (!dataFile.exists()) dataFile.createFile()
}

/**
 * importarFitxer -> funció que importa les dades d'un fitxer a la fitxer userData
 * @param path -> variable que conté el path del fitxer del que s'exporten les dades
 */
fun importarFitxer(path: Path) {
    val dataFile = Path("dataFiles/data/userData.txt")        //Obrim el fitxer userData
    var id:Int                                                  //Variable id que guarda l'id de l'últim usuari
    if (!dataFile.exists()) {                                   //Si el fitxer userData no existeix, el crea i posa id = 1
        dataFile.createFile()
        id = 1
    } else {                                                    //Si el fitxer existeix, obtinc l'id de l'últim usuari
        val liniesData = dataFile.readLines()
        id = liniesData[liniesData.size-1].split(";")[0].toInt()+1
    }
    var text: String = path.readText()              //Guardem el contingut del fitxer a la variable text i eliminem els salts de línia que hi pugui haver
    text = text.replace("\n", "")
    var usuaris = text.split(";")           //dividim el text en un array amb el separador ;
    for (i in usuaris.indices) {                        //per cada element de l'array afegim un usuari
        var nouUsuari = usuaris[i].replace(",", ";")
        val index = nouUsuari.indexOf(';')
        nouUsuari = id.toString().plus(nouUsuari.substring(index))
        if (i == 0 && id > 1) dataFile.appendText("\n")
        dataFile.appendText("$nouUsuari")
        if (i < usuaris.size-1) dataFile.appendText("\n")
        id++
    }
}